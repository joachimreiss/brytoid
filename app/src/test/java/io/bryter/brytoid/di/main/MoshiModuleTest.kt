package io.bryter.brytoid.di.main

import io.bryter.brytoid.models.ClientContent
import org.junit.Test

class MoshiModuleTest {
    val moshi = MoshiModule().provideMoshi()
    val clientContentAdapter = moshi.adapter(ClientContent::class.java)

    @Test
    fun singleInputs() {
        val fromJson = clientContentAdapter.fromJson(
            """
                {
                  "type": "InProgress",
                  "pages": [
                    {
                      "type": "Input",
                      "id": "node_amount_legs",
                      "html": "<h1>Two or Four legs?</h1><p>&nbsp;</p><p> <strong>Welcome to your favorite animal finder! </strong></p><p>&nbsp;</p><p>Which animals do you find cuter - those with two legs or with four legs?</p>",
                      "input": {
                        "type": "SingleChoice",
                        "request": {
                          "choices": [
                            {
                              "id": "choice_id_8576dfadd15ba881f1df",
                              "label": "Less Than Four"
                            },
                            {
                              "id": "choice_id_87a1c43b34c9e5a63370",
                              "label": "Four"
                            }
                          ],
                          "optional": false,
                          "isDropdown": false,
                          "isCheckbox": false
                        },
                        "response": null
                      }
                    },
                    {
                      "type": "Input",
                      "id": "asdf",
                      "html": "<p>asdf!</p>",
                      "input": {
                        "type": "Text",
                        "request": {
                          "optional": false,
                          "placeholderContent": "my placeholder",
                          "allowLinebreaks": false
                        },
                        "response": null
                      }
                    }
                  ]
                }
            """.trimIndent()
        )

        println(fromJson)


        val toJson = clientContentAdapter.toJson(fromJson)

        println(toJson)
    }

    @Test
    fun multiInput() {
        val fromJson = clientContentAdapter.fromJson(
            """
            {
    "type": "InProgress",
    "pages": [
      {
        "type": "Input",
        "id": "node_amount_legs",
        "html": "<h1>Two or Four legs?</h1><p>&nbsp;</p><p> <strong>Welcome to your favorite animal finder! </strong></p><p>&nbsp;</p><p>Which animals do you find cuter - those with two legs or with four legs?</p>",
        "input": {
          "type": "SingleChoice",
          "request": {
            "choices": [
              {
                "id": "choice_id_8576dfadd15ba881f1df",
                "label": "Less Than Four"
              },
              {
                "id": "choice_id_87a1c43b34c9e5a63370",
                "label": "Four"
              }
            ],
            "optional": false,
            "isDropdown": false,
            "isCheckbox": false
          },
          "response": {
            "chosenId": "choice_id_8576dfadd15ba881f1df"
          }
        }
      },
      {
        "type": "Input",
        "id": "user_input_id_59c544c03cec37cdebda",
        "html": "<p>&nbsp;</p>",
        "input": {
          "type": "FileUpload",
          "request": {
            "placeholderUploadButtonText": "",
            "fileCategory": "image",
            "contentTypes": [
              "image/jpeg",
              "image/png",
              "image/webp",
              "image/gif"
            ],
            "optional": false
          },
          "response": {
            "blobId": "qm_53gCHRC-NF_amGtrqzQ",
            "contentType": "image/png",
            "fileName": "Screen Shot 2020-03-25 at 1.30.27 PM.png",
            "size": 10645
          }
        }
      },
      {
        "type": "Input",
        "id": "user_input_id_67e11c5541b244b6f694",
        "html": "<p>as;ldkas;lkd;laskd;laskda</p><p>sda</p><p>sd</p><p>asd</p><p>as</p><p>dasasda</p><p>sd</p><p>as</p><p>da</p><p>sd</p><p>asd</p><p>as</p><p>d</p><p>da</p><p>sd</p><p>&nbsp;</p><p><span><span class=\"image-wrapper\"><img src=\"/api/v1/wizard/Rp7MJMmTRKK0EkGi5PN7Zw/sessions/p0eJ8FGPThWAOrs-snbeLA/blobs/qm_53gCHRC-NF_amGtrqzQ\" alt=\"New Input\"></span></span></p><p>&nbsp;</p>",
        "input": {
          "type": "Text",
          "request": {
            "placeholderContent": "",
            "allowLinebreaks": false,
            "optional": true
          },
          "response": {
            "text": "qqq"
          }
        }
      },
      {
        "type": "Input",
        "id": "node_multi_input_single_choice",
        "html": "<p>New Node</p><p>&nbsp;</p><p><span data-v-83d6d2a0=\"\" data-target-node-id=\"node_amount_legs\" data-type=\"wizard-page-link\" class=\"wizard-page-link\" contenteditable=\"false\"><strong>Two or Four legs?</strong></span></p><p>&nbsp;</p><p><span>8.9</span></p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p><span data-v-83d6d2a0=\"\" data-target-node-id=\"node_amount_legs\" data-type=\"wizard-page-link\" class=\"wizard-page-link\" contenteditable=\"false\"><strong>Two or Four legs</strong></span></p><p>&nbsp;</p>",
        "input": {
          "type": "MultiInput",
          "pages": [
            {
              "type": "Input",
              "id": "var_id_multi_input_single_choice.var_id_e3108840b7df58bf8fa8",
              "html": "Select one",
              "input": {
                "type": "SingleChoice",
                "request": {
                  "choices": [
                    {
                      "id": "choice_id_4f9d0f9a3c153ad092ab",
                      "label": "Yes"
                    },
                    {
                      "id": "choice_id_c98e70cd2fc163040a99",
                      "label": "No"
                    },
                    {
                      "id": "choice_id_d270f413fd843bdf701a",
                      "label": "Maybe"
                    }
                  ],
                  "optional": false,
                  "isDropdown": false,
                  "isCheckbox": false
                },
                "response": null
              }
            }
          ]
        }
      }
    ]
  }
        """.trimIndent()
        )


        println(fromJson)
        val toJson = clientContentAdapter.toJson(fromJson)
        println(toJson)
    }

    @Test
    fun resultContent() {
        val fromJson = clientContentAdapter.fromJson(
            """
            {
                "type": "Finished",
                "page": {
                  "type": "Result",
                  "html": "asdf"
                }
            }
            """.trimIndent()
        )
        println(fromJson)
        val toJson = clientContentAdapter.toJson(fromJson)
        println(toJson)
    }
}