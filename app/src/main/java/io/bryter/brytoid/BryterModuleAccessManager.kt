package io.bryter.brytoid

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import io.bryter.brytoid.models.ModuleAccess
import io.bryter.brytoid.ui.auth.AuthResource
import io.bryter.brytoid.ui.auth.AuthResource.Companion.loading
import io.bryter.brytoid.ui.auth.AuthResource.Companion.logout
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BryterModuleAccessManager @Inject constructor() {

    private val cachedModuleAccess =
        MediatorLiveData<AuthResource<ModuleAccess?>>()

    fun accessModule(source: MutableLiveData<AuthResource<ModuleAccess?>>) {
        cachedModuleAccess.value = loading(null)
        cachedModuleAccess.addSource(source) {
            cachedModuleAccess.value = it
            cachedModuleAccess.removeSource(source)
            if (it.status == AuthResource.AuthStatus.ERROR) {
                cachedModuleAccess.value = logout()
            }
        }
    }

    fun releaseModuleAccess() {
        cachedModuleAccess.value = logout()
    }

    val currentModuleAccess: LiveData<AuthResource<ModuleAccess?>> get() = cachedModuleAccess

    val currentPublishedModuleId: String?
        get() {
            val value = currentModuleAccess.value
            return if (value != null && value.status == AuthResource.AuthStatus.AUTHENTICATED && value.data != null) {
                value.data.publishedModuleId
            } else null
        }
}