package io.bryter.brytoid

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import dagger.android.support.DaggerAppCompatActivity
import io.bryter.brytoid.ui.auth.AuthResource.AuthStatus
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    companion object {
        private const val TAG = "DaggerExample"
    }

    @JvmField
    @Inject
    var bryterModuleAccessManager: BryterModuleAccessManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        subscribeObservers()
    }

    private fun subscribeObservers() {
        bryterModuleAccessManager!!.currentModuleAccess
            .observe(this, Observer { userAuthResource ->
                if (userAuthResource != null) {
                    when (userAuthResource.status) {
                        AuthStatus.LOADING -> {
                            Log.d(
                                TAG,
                                "onChanged: BaseActivity: LOADING..."
                            )
                        }
                        AuthStatus.AUTHENTICATED -> {
                            Log.d(
                                TAG,
                                "onChanged: BaseActivity: AUTHENTICATED... " +
                                        "Authenticated as: " + userAuthResource.data.toString()
                            )
                        }
                        AuthStatus.ERROR -> {
                            Log.d(
                                TAG,
                                "onChanged: BaseActivity: ERROR..."
                            )
                        }
                        AuthStatus.NOT_AUTHENTICATED -> {
                            Log.d(
                                TAG,
                                "onChanged: BaseActivity: NOT AUTHENTICATED. Navigating to Login screen."
                            )
                            navLoginScreen()
                        }
                    }
                }
            })
    }

    private fun navLoginScreen() {
//        Intent intent = new Intent(this, AuthActivity.class);
//        startActivity(intent);
//        finish();
    }
}