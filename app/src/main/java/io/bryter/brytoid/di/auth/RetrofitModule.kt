package io.bryter.brytoid.di.auth

import dagger.Module
import dagger.Provides
import io.bryter.brytoid.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named

@Module
class RetrofitModule {

    @AuthScope
    @Provides
    @Named("auth")
    fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
}