package io.bryter.brytoid.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.bryter.brytoid.di.auth.AuthModule
import io.bryter.brytoid.di.auth.AuthScope
import io.bryter.brytoid.di.auth.AuthViewModelsModule
import io.bryter.brytoid.di.auth.RetrofitModule
import io.bryter.brytoid.di.main.*
import io.bryter.brytoid.ui.auth.AuthActivity
import io.bryter.brytoid.ui.main.MainActivity

@Module
abstract class ActivityBuildersModule {
    @AuthScope
    @ContributesAndroidInjector(modules = [RetrofitModule::class, AuthViewModelsModule::class, AuthModule::class])
    abstract fun contributeAuthActivity(): AuthActivity

    @MainScope
    @ContributesAndroidInjector(modules = [MoshiModule::class, OkHttpModule::class, MainRetrofitModule::class, MainFragmentBuildersModule::class, MainViewModelsModule::class, MainModule::class])
    abstract fun contributeMainActivity(): MainActivity
}