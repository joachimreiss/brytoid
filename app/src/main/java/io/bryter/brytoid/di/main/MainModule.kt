package io.bryter.brytoid.di.main

import dagger.Module
import dagger.Provides
import io.bryter.brytoid.network.main.MainApi
import io.bryter.brytoid.ui.main.PublicEngineStateManager
import retrofit2.Retrofit
import javax.inject.Named

@Module
class MainModule {

    @MainScope
    @Provides
    fun providePublicEngineStateManager(): PublicEngineStateManager = PublicEngineStateManager()

    @MainScope
    @Provides
    fun provideMainApi(@Named("main") retrofit: Retrofit): MainApi =
        retrofit.create(MainApi::class.java)
}