package io.bryter.brytoid.di.main

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import dagger.Module
import dagger.Provides
import io.bryter.brytoid.models.*
import java.util.*


@Module
class MoshiModule {

    @Provides
    @MainScope
    fun provideMoshi(): Moshi = createMoshi()
}

private fun createMoshi(): Moshi = Moshi.Builder()
    .add(FileUploadCategoryAdapter())
    .add(ContentTypeAdapter())
    .add(
        PolymorphicJsonAdapterFactory.of(ClientFinishedPage::class.java, "type")
            .withSubtype(ClientResultPage::class.java, ClientPageType.Result.toString())
            .withSubtype(
                ClientRedirectPage::class.java,
                ClientPageType.Redirect.toString()
            )
            .withSubtype(
                ClientHandoverSentPage::class.java,
                ClientPageType.HandoverSent.toString()
            )
    )
    .add(
        PolymorphicJsonAdapterFactory.of(ClientInput::class.java, "type")
            .withSubtype(
                SingleChoiceClientInput::class.java,
                ClientInputType.SingleChoice.toString()
            )
            .withSubtype(
                MultiSelectClientInput::class.java,
                ClientInputType.MultiSelect.toString()
            )
            .withSubtype(TextClientInput::class.java, ClientInputType.Text.toString())
            .withSubtype(
                EmailAddressClientInput::class.java,
                ClientInputType.EmailAddress.toString()
            )
            .withSubtype(
                NumberClientInput::class.java,
                ClientInputType.Number.toString()
            )
            .withSubtype(
                FileUploadClientInput::class.java,
                ClientInputType.FileUpload.toString()
            )
            .withSubtype(
                DatetimeClientInput::class.java,
                ClientInputType.Datetime.toString()
            )
            .withSubtype(
                MultiClientInput::class.java,
                ClientInputType.MultiInput.toString()
            )
    )
    .add(
        PolymorphicJsonAdapterFactory.of(ClientInProgressPage::class.java, "type")
            .withSubtype(ClientInputPage::class.java, ClientPageType.Input.toString())
            .withSubtype(
                ClientReadyForSubmissionPage::class.java,
                ClientPageType.ReadyForSubmission.toString()
            )
    )
    .add(
        PolymorphicJsonAdapterFactory.of(ClientContent::class.java, "type")
            .withSubtype(
                ClientInProgressContent::class.java,
                ClientContentType.InProgress.toString()
            )
            .withSubtype(
                ClientFinishedContent::class.java,
                ClientContentType.Finished.toString()
            )
    )
    .build()

private class FileUploadCategoryAdapter {
    @ToJson
    fun toJson(fileUploadCategory: FileUploadCategory): String =
        fileUploadCategory.toString().toLowerCase(Locale.ENGLISH)

    @FromJson
    fun fromJson(fileUploadCategory: String): FileUploadCategory =
        FileUploadCategory.values().find {
            it.toString()
                .toLowerCase(Locale.ENGLISH) == fileUploadCategory.toLowerCase(Locale.ENGLISH)
        }
            ?: throw JsonDataException("unknown file upload category $fileUploadCategory")
}

private class ContentTypeAdapter {
    @ToJson
    fun toJson(contentType: ContentType): String = contentType.value

    @FromJson
    fun fromJson(contentType: String): ContentType = ContentType.values().find {
        it.value == contentType
    } ?: throw JsonDataException("unknown content type $contentType")
}