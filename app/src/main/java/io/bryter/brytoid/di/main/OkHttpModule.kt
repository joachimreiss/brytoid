package io.bryter.brytoid.di.main

import com.moczul.ok2curl.CurlInterceptor
import com.moczul.ok2curl.logger.Loggable
import dagger.Module
import dagger.Provides
import io.bryter.brytoid.BryterModuleAccessManager
import io.bryter.brytoid.network.AcceptHeadersInterceptor
import io.bryter.brytoid.network.AuthorizeInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

@Module
class OkHttpModule {
    @MainScope
    @Provides
    fun provideOkHttpClient(bryterModuleAccessManager: BryterModuleAccessManager): OkHttpClient =
        OkHttpClient.Builder().build().newBuilder()
            .addInterceptor(AcceptHeadersInterceptor())
            .addInterceptor(AuthorizeInterceptor(bryterModuleAccessManager))
            .addInterceptor(CurlInterceptor(Loggable { message -> println(message) }))
            .addInterceptor(HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) })
            .build()
}