package io.bryter.brytoid.di.auth

import dagger.Module
import dagger.Provides
import io.bryter.brytoid.models.ModuleAccess
import io.bryter.brytoid.network.auth.AuthApi
import retrofit2.Retrofit
import javax.inject.Named

@Module
class AuthModule {
    @AuthScope
    @Provides
    @Named("published_module")
    fun providePublishedModule(): ModuleAccess = ModuleAccess("-1", "-1")

    @AuthScope
    @Provides
    fun provideAuthApi(@Named("auth") retrofit: Retrofit): AuthApi =
        retrofit.create(AuthApi::class.java)
}