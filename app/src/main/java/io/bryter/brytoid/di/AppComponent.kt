package io.bryter.brytoid.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.bryter.brytoid.BaseApplication
import io.bryter.brytoid.BryterModuleAccessManager
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ActivityBuildersModule::class, ViewModelFactoryModule::class])
interface AppComponent : AndroidInjector<BaseApplication> {
    fun bryterModuleAccessManager(): BryterModuleAccessManager

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }
}