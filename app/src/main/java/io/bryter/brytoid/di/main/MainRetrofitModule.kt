package io.bryter.brytoid.di.main

import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.bryter.brytoid.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named

@Module
class MainRetrofitModule {
    @MainScope
    @Provides
    @Named("main")
    fun provideMainRetrofit(moshi: Moshi, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
}