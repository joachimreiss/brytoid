package io.bryter.brytoid.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import io.bryter.brytoid.models.ApiResource
import io.bryter.brytoid.models.PublicEngineState
import io.bryter.brytoid.ui.main.pages.PageItem
import io.bryter.brytoid.ui.main.pages.clientContentToPageItems
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PublicEngineStateManager @Inject constructor() {

    private val mediatorLiveData = MediatorLiveData<ApiResource<PublicEngineState>>()

    fun handleRequest(source: MutableLiveData<ApiResource<PublicEngineState>>) {
        val oldData = mediatorLiveData.value?.data
        mediatorLiveData.value = ApiResource.loading(oldData)
        mediatorLiveData.addSource(source, fun(it: ApiResource<PublicEngineState>) {
            mediatorLiveData.value = it
            mediatorLiveData.removeSource(source)
        })
    }

    val stateLiveData: LiveData<ApiResource<PublicEngineState>> get() = mediatorLiveData
    val pages: List<PageItem>
        get() = stateLiveData.value?.data?.content?.let {
            clientContentToPageItems(it)
        } ?: emptyList()

}