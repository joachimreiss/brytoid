package io.bryter.brytoid.ui.main.pages

import io.bryter.brytoid.models.Choice
import io.bryter.brytoid.models.ContentType
import io.bryter.brytoid.models.FileUploadCategory
import io.bryter.brytoid.models.HandoverSentStatus
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.MultiInputPageInput
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputRequest.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse.*

interface PageItem {
    fun stableId(): Long
}

data class ResultPageItem(val html: String) : PageItem {

    val id = html.hashCode().toLong()

    override fun stableId(): Long {
        return id
    }
}

data class RedirectPageItem(val url: String) : PageItem {
    val id = url.hashCode().toLong()

    override fun stableId(): Long {
        return id
    }
}

data class HandoverSentPageItem(val html: String, val handoverSentStatus: HandoverSentStatus) :
    PageItem {
    val id = html.hashCode().toLong() + handoverSentStatus.hashCode().toLong()

    override fun stableId(): Long {
        return id
    }
}

object ReadyForSubmissionPageItem : PageItem {
    val id = this.javaClass.hashCode().toLong()

    override fun stableId(): Long {
        return id
    }
}

sealed class InputPageItem(
    open val id: String,
    open val html: String,
    open val input: InputPageInput
) : PageItem {

    override fun stableId(): Long {
        return id.hashCode().toLong()
    }

    data class MultiInputPageItem(
        override val id: String,
        override val html: String,
        override val input: MultiInputPageInput
    ) : InputPageItem(id, html, input)

    sealed class SingleInputPageItem(
        override val id: String,
        override val html: String,
        override val input: SingleInputPageInput
    ) : InputPageItem(id, html, input) {
        data class SingleChoiceInputPageItem(
            override val id: String,
            override val html: String,
            override val input: SingleChoiceInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class MultiSelectInputPageItem(
            override val id: String,
            override val html: String,
            override val input: MultiSelectInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class TextInputPageItem(
            override val id: String,
            override val html: String,
            override val input: TextInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class EmailAddressInputPageItem(
            override val id: String,
            override val html: String,
            override val input: EmailAddressInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class NumberInputPageItem(
            override val id: String,
            override val html: String,
            override val input: NumberInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class FileUploadInputPageItem(
            override val id: String,
            override val html: String,
            override val input: FileUploadInputPageInput
        ) : SingleInputPageItem(id, html, input)

        data class DateInputPageItem(
            override val id: String,
            override val html: String,
            override val input: DatetimeInputPageInput
        ) : SingleInputPageItem(id, html, input)
    }

    sealed class InputPageInput {
        data class MultiInputPageInput(val inputs: List<InputPageInput>) : InputPageInput()

        sealed class SingleInputPageInput(
            open val request: SingleInputPageInputRequest,
            open val response: SingleInputPageInputResponse?
        ) : InputPageInput() {
            data class SingleChoiceInputPageInput(
                override val request: SingleChoiceInputPageInputRequest,
                override val response: SingleChoiceInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class MultiSelectInputPageInput(
                override val request: MultiSelectInputPageInputRequest,
                override val response: MultiSelectInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class TextInputPageInput(
                override val request: TextInputPageInputRequest,
                override val response: TextInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class EmailAddressInputPageInput(
                override val request: EmailAddressInputPageInputRequest,
                override val response: EmailAddressInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class NumberInputPageInput(
                override val request: NumberInputPageInputRequest,
                override val response: NumberInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class FileUploadInputPageInput(
                override val request: FileUploadInputPageInputRequest,
                override val response: FileUploadInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            data class DatetimeInputPageInput(
                override val request: DatetimeInputPageInputRequest,
                override val response: DatetimeInputPageInputResponse?
            ) : SingleInputPageInput(request, response)

            sealed class SingleInputPageInputRequest(open val optional: Boolean) {
                data class SingleChoiceInputPageInputRequest(
                    val choices: List<Choice>,
                    val isDropdown: Boolean,
                    val isCheckbox: Boolean,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class MultiSelectInputPageInputRequest(
                    val choices: List<Choice>,
                    val isDropdown: Boolean,
                    val isCheckbox: Boolean,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class TextInputPageInputRequest(
                    val placeholderContent: String,
                    val regex: String?,
                    val characterLimit: Int?,
                    val allowLinebreaks: Boolean,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class EmailAddressInputPageInputRequest(
                    val placeholderEmailAddress: String,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class NumberInputPageInputRequest(
                    val placeholderNumber: String,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class FileUploadInputPageInputRequest(
                    val placeholderUploadButtonText: String,
                    val fileCategory: FileUploadCategory,
                    val contentTypes: List<ContentType>,
                    val allowMultipleFiles: Boolean,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)

                data class DatetimeInputPageInputRequest(
                    val datetimeFormat: String,
                    override val optional: Boolean
                ) : SingleInputPageInputRequest(optional)
            }

            sealed class SingleInputPageInputResponse() {
                data class SingleChoiceInputPageInputResponse(
                    val chosenId: String?
                ) : SingleInputPageInputResponse()

                data class MultiSelectInputPageInputResponse(
                    val chosenIds: List<String>?
                ) : SingleInputPageInputResponse()

                data class TextInputPageInputResponse(
                    val text: String?
                ) : SingleInputPageInputResponse()

                data class EmailAddressInputPageInputResponse(
                    val emailAddress: String?
                ) : SingleInputPageInputResponse()

                data class NumberInputPageInputResponse(
                    val number: Float?
                ) : SingleInputPageInputResponse()

                data class FileUploadInputPageInputResponse(
                    val blobId: String?,
                    val fileName: String?,
                    val contentType: ContentType?,
                    val size: Float?
                ) : SingleInputPageInputResponse()

                data class DatetimeInputPageInputResponse(
                    val datetime: String?
                ) : SingleInputPageInputResponse()
            }
        }
    }
}
