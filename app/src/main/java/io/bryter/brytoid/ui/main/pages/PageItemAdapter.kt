package io.bryter.brytoid.ui.main.pages

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.bryter.brytoid.R
import io.bryter.brytoid.ui.main.pages.InputPageItem.MultiInputPageItem
import io.bryter.brytoid.ui.main.pages.InputPageItem.SingleInputPageItem.*
import io.bryter.brytoid.ui.main.pages.TextClientInputPageInputViewHolder.*

class PageItemAdapter(val listener: SendNewResponseListener) :
    RecyclerView.Adapter<PageItemViewHolder>() {

    init {
        setHasStableIds(true)
    }

    private var list = emptyList<PageItem>()

    internal fun updateContent(list: List<PageItem>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long = getItem(position).stableId()

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is ResultPageItem -> R.layout.page_item_result
        is RedirectPageItem -> R.layout.page_item_redirect
        is HandoverSentPageItem -> R.layout.page_item_handover_sent
        is ReadyForSubmissionPageItem -> R.layout.page_item_ready_for_submission
        is SingleChoiceInputPageItem -> R.layout.page_item_single_choice
        is MultiSelectInputPageItem -> R.layout.page_item_multi_select
        is TextInputPageItem -> R.layout.page_item_text
        is EmailAddressInputPageItem -> R.layout.page_item_email_address
        is NumberInputPageItem -> R.layout.page_item_number
        is FileUploadInputPageItem -> R.layout.page_item_file_upload
        is DateInputPageItem -> R.layout.page_item_date
        is MultiInputPageItem -> R.layout.page_item_multi_input
        else -> -1
    }

    override fun onCreateViewHolder(group: ViewGroup, viewType: Int): PageItemViewHolder {
        val layout = LayoutInflater.from(group.context).inflate(viewType, group, false)
        return when (viewType) {
            R.layout.page_item_result -> ResultPageItemViewHolder(layout)
            R.layout.page_item_redirect -> RedirectPageItemViewHolder(layout)
            R.layout.page_item_handover_sent -> HandoverSentPageItemViewHolder(layout)
            R.layout.page_item_ready_for_submission -> ReadyForSubmissionPageItemViewHolder(layout)
            R.layout.page_item_single_choice -> SingleChoiceInputPageInputViewHolder(
                layout,
                listener
            )
            R.layout.page_item_multi_select -> MultiSelectInputPageInputViewHolder(layout, listener)
            R.layout.page_item_text -> TextClientInputPageInputViewHolder(layout, listener)
            R.layout.page_item_email_address -> EmailAddressInputPageItemViewHolder(
                layout,
                listener
            )
            R.layout.page_item_number -> NumberInputPageItemViewHolder(layout, listener)
            R.layout.page_item_file_upload -> FileUploadInputPageItemViewHolder(layout, listener)
            R.layout.page_item_date -> DatePageItemViewHolder(layout, listener)
            R.layout.page_item_multi_input -> MultiInputPageItemViewHolder(layout, listener)
            else -> throw IllegalStateException("Invalid viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: PageItemViewHolder, position: Int) {
        holder.bindView(getItem(position))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    private fun getItem(position: Int): PageItem {
        return list[position]
    }

}
