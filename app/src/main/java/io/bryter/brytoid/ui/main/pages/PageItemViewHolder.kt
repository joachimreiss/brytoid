package io.bryter.brytoid.ui.main.pages

import android.text.util.Linkify
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import io.bryter.brytoid.R
import io.bryter.brytoid.ui.main.WebViewWithProgressBar
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.MultiInputPageInput
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse.SingleChoiceInputPageInputResponse
import io.bryter.brytoid.ui.main.pages.InputPageItem.MultiInputPageItem
import io.bryter.brytoid.ui.main.pages.InputPageItem.SingleInputPageItem.*

private fun View.loadHtml(html: String) {
    this.findViewById<WebViewWithProgressBar>(R.id.web_view_with_progress_bar)?.html = html
}

abstract class PageItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun bindView(item: PageItem)

}

class ResultPageItemViewHolder(private val layout: View) : PageItemViewHolder(layout) {

    private var item: ResultPageItem? = null

    override fun bindView(item: PageItem) {
        if (item is ResultPageItem) {
            this.item = item
            layout.loadHtml(item.html)
        }
    }

}

class RedirectPageItemViewHolder(private val layout: View) : PageItemViewHolder(layout) {

    private var item: RedirectPageItem? = null

    override fun bindView(item: PageItem) {
        if (item is RedirectPageItem) {
            this.item = item
            layout.findViewById<TextView>(R.id.url).let {
                it.text = item.url
                Linkify.addLinks(it, Linkify.WEB_URLS)
            }
        }
    }
}

class HandoverSentPageItemViewHolder(private val layout: View) : PageItemViewHolder(layout) {

    private var item: HandoverSentPageItem? = null

    override fun bindView(item: PageItem) {
        if (item is HandoverSentPageItem) {
            this.item = item
            layout.loadHtml(item.html)
            val status = layout.findViewById<TextView>(R.id.handover_sent_status)
            status.text = item.handoverSentStatus.toString()
        }
    }
}

class ReadyForSubmissionPageItemViewHolder(private val layout: View) : PageItemViewHolder(layout) {

    override fun bindView(item: PageItem) {
    }

}

class SingleChoiceInputPageInputViewHolder(
    private val layout: View,
    private val listener: SendNewResponseListener
) : PageItemViewHolder(layout) {

    private var item: SingleChoiceInputPageItem? = null

    private var checkedIndex = -1

    override fun bindView(item: PageItem) {
        if (item is SingleChoiceInputPageItem) {
            this.item = item
            layout.loadHtml(item.html)

            val choices = item.input.request.choices

            val chosenId = item.input.response?.chosenId

            val next = layout.findViewById<Button>(R.id.next)

            val radioGroup = layout.findViewById<RadioGroup>(R.id.radio_group)
            radioGroup.apply {
                removeAllViews()
                choices.forEachIndexed { index, choice ->
                    addView(RadioButton(layout.context).apply {
                        id = index
                        text = choice.label
                        isChecked = chosenId != null && chosenId == choice.id
                        setOnCheckedChangeListener { _: RadioGroup, checkedId: Int ->
                            next.isEnabled = radioGroup.checkedRadioButtonId != -1
                        }
                    })

                }
            }

            next.isEnabled = radioGroup.checkedRadioButtonId != -1

            fun selectedChoiceId(): String? {
                radioGroup.findViewById<RadioButton>(radioGroup.checkedRadioButtonId)
                    ?.let { radioButton ->
                        radioGroup.indexOfChild(radioButton).let { index ->
                            if (index >= 0 && index < choices.size) {
                                return item.input.request.choices[index].id
                            }
                        }
                    }
                return null
            }

            next.setOnClickListener {
                listener.sendNewResponse(
                    item.id,
                    SingleChoiceInputPageInputResponse(selectedChoiceId())
                )
            }
        }
    }
}

class MultiSelectInputPageInputViewHolder(
    private val layout: View,
    private val listener: SendNewResponseListener
) :
    PageItemViewHolder(layout) {

    private var item: MultiSelectInputPageItem? = null

    override fun bindView(item: PageItem) {
        if (item is MultiSelectInputPageItem) {
            this.item = item
            layout.loadHtml(item.html)


            val chosenIds = item.input.response?.chosenIds
            layout.findViewById<RadioGroup>(R.id.checkbox_group).apply {
                removeAllViews()
                item.input.request.choices.forEach {
                    addView(RadioButton(layout.context).apply {
                        text = it.label
                        isChecked = chosenIds != null && chosenIds.contains(it.id)
                    })
                }
            }
        }
    }
}

class TextClientInputPageInputViewHolder(
    private val layout: View,
    private val listener: SendNewResponseListener
) :
    PageItemViewHolder(layout) {

    private var item: TextInputPageItem? = null

    override fun bindView(item: PageItem) {
        if (item is TextInputPageItem) {
            this.item = item
            layout.loadHtml(item.html)
            val text = item.input.response?.text ?: ""
            layout.findViewById<EditText>(R.id.edit_text).apply {
                setText(text)
            }
        }
    }

    class EmailAddressInputPageItemViewHolder(
        private val layout: View,
        private val listener: SendNewResponseListener
    ) :
        PageItemViewHolder(layout) {

        private var item: EmailAddressInputPageItem? = null

        override fun bindView(item: PageItem) {
            if (item is EmailAddressInputPageItem) {
                this.item = item
                layout.loadHtml(item.html)
                val emailAddress = item.input.response?.emailAddress ?: ""
                layout.findViewById<EditText>(R.id.edit_text).apply {
                    setText(emailAddress)
                }
            }
        }
    }

    class NumberInputPageItemViewHolder(
        private val layout: View,
        private val listener: SendNewResponseListener
    ) : PageItemViewHolder(layout) {

        private var item: NumberInputPageItem? = null

        override fun bindView(item: PageItem) {
            if (item is NumberInputPageItem) {
                this.item = item
                layout.loadHtml(item.html)
                val number = item.input.response?.number
                layout.findViewById<EditText>(R.id.edit_text).apply {
                    setText(number?.toString() ?: "")
                }
            }
        }
    }

    class FileUploadInputPageItemViewHolder(
        private val layout: View,
        private val listener: SendNewResponseListener
    ) : PageItemViewHolder(layout) {

        private var item: FileUploadInputPageItem? = null

        override fun bindView(item: PageItem) {
            if (item is FileUploadInputPageItem) {
                this.item = item
                layout.loadHtml(item.html)
                TODO()
            }
        }
    }

    class DatePageItemViewHolder(
        private val layout: View,
        private val listener: SendNewResponseListener
    ) : PageItemViewHolder(layout) {

        private var item: DateInputPageItem? = null

        override fun bindView(item: PageItem) {
            if (item is DateInputPageItem) {
                this.item = item
                layout.loadHtml(item.html)
                TODO()
            }
        }
    }

    class MultiInputPageItemViewHolder(
        private val layout: View,
        private val listener: SendNewResponseListener
    ) : PageItemViewHolder(layout) {

        private var item: MultiInputPageItem? = null

        override fun bindView(item: PageItem) {
            if (item is MultiInputPageItem) {
                this.item = item
                layout.loadHtml(item.html)
                layout.findViewById<ViewGroup>(R.id.multi_input_container).apply {
                    removeAllViews()
                    item.input.inputs.forEach {
                        when (it) {
                            is MultiInputPageInput -> TODO()
                            is SingleChoiceInputPageInput -> TODO()
                            is MultiSelectInputPageInput -> TODO()
                            is TextInputPageInput -> TODO()
                            is EmailAddressInputPageInput -> TODO()
                            is NumberInputPageInput -> TODO()
                            is FileUploadInputPageInput -> TODO()
                            is DatetimeInputPageInput -> TODO()
                        }
                    }
                }
            }
        }
    }
}