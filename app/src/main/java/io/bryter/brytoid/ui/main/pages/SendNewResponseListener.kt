package io.bryter.brytoid.ui.main.pages

import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse

interface SendNewResponseListener {

    fun sendNewResponse(pageId: String, response: SingleInputPageInputResponse)

}