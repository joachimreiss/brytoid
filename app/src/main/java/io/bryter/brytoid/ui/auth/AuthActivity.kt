package io.bryter.brytoid.ui.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import io.bryter.brytoid.DEBUG_PUBLISHED_MODULE_ID
import io.bryter.brytoid.R
import io.bryter.brytoid.di.ViewModelProviderFactory
import io.bryter.brytoid.models.ModuleAccess
import io.bryter.brytoid.ui.auth.AuthResource.AuthStatus
import io.bryter.brytoid.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_auth.*
import javax.inject.Inject
import javax.inject.Named

class AuthActivity : DaggerAppCompatActivity() {

    private lateinit var viewModel: AuthViewModel

    @JvmField
    @Inject
    var providerFactory: ViewModelProviderFactory? = null

    @JvmField
    @Inject
    @Named("published_module")
    var publishedModule: ModuleAccess? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        login_button.setOnClickListener {
            startDebugSession()
        }
        viewModel =
            ViewModelProvider(this, providerFactory!!).get(AuthViewModel::class.java)
        subscribeObservers()
    }

    private fun subscribeObservers() {
        viewModel.observeAuthState()
            .observe(this, Observer { userAuthResource ->
                if (userAuthResource != null) {
                    when (userAuthResource.status) {
                        AuthStatus.LOADING -> {
                            showProgressBar(true)
                        }
                        AuthStatus.AUTHENTICATED -> {
                            showProgressBar(false)
                            onLoginSuccess()
                        }
                        AuthStatus.ERROR -> {
                            showProgressBar(false)
                            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                        }
                        AuthStatus.NOT_AUTHENTICATED -> {
                            showProgressBar(false)
                        }
                    }
                }
            })
    }

    private fun onLoginSuccess() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun showProgressBar(isVisible: Boolean) {
        if (isVisible) {
            progress_bar.visibility = View.VISIBLE
        } else {
            progress_bar.visibility = View.INVISIBLE
        }
    }

    private fun startDebugSession() {
        viewModel.accessPublishedModule(DEBUG_PUBLISHED_MODULE_ID)
    }
}