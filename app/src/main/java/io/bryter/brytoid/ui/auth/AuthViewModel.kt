package io.bryter.brytoid.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.bryter.brytoid.BryterModuleAccessManager
import io.bryter.brytoid.models.ModuleAccess
import io.bryter.brytoid.models.OAuthLoginRequestBody
import io.bryter.brytoid.models.OAuthLoginResponseBody
import io.bryter.brytoid.network.auth.AuthApi
import io.bryter.brytoid.ui.auth.AuthResource.Companion.authenticated
import io.bryter.brytoid.ui.auth.AuthResource.Companion.error
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class AuthViewModel @Inject constructor(
    private val authApi: AuthApi,
    private val bryterModuleAccessManager: BryterModuleAccessManager
) : ViewModel() {

    fun observeAuthState(): LiveData<AuthResource<ModuleAccess?>> {
        return bryterModuleAccessManager.currentModuleAccess
    }

    fun accessPublishedModule(publishedModuleId: String) {
        bryterModuleAccessManager.accessModule(auth(publishedModuleId))
    }

    private fun auth(publishedModuleId: String): MutableLiveData<AuthResource<ModuleAccess?>> {
        val liveData = MutableLiveData<AuthResource<ModuleAccess?>>()
        val auth = authApi.auth(OAuthLoginRequestBody(publishedModuleId))
        auth.enqueue(object : Callback<OAuthLoginResponseBody> {
            override fun onResponse(
                call: Call<OAuthLoginResponseBody>,
                response: Response<OAuthLoginResponseBody>
            ) {
                val body = response.body()
                    ?: throw IllegalStateException("Empty body on successful response")
                val authenticated = authenticated(ModuleAccess(publishedModuleId, body.accessToken))
                liveData.value = authenticated
            }

            override fun onFailure(call: Call<OAuthLoginResponseBody>, t: Throwable) {
                t.printStackTrace()
                liveData.value = error("error", null)
            }
        })
        return liveData
    }
}