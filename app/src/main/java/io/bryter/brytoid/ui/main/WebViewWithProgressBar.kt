package io.bryter.brytoid.ui.main

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.RelativeLayout
import io.bryter.brytoid.BASE_URL
import io.bryter.brytoid.R

class WebViewWithProgressBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : RelativeLayout(context, attrs, defStyle, defStyleRes) {

    private val adjustImagesCss =
        "<style>img{display: inline;height: auto;max-width: 100%;}</style>"
    private val mimeType = "text/html; charset=utf-8"
    private val encoding = "UTF-8"

    var html: String = ""
        set(value) {
            if (field != value) {
                field = value
                updateUI()
            }
        }

    private var progressBar: ProgressBar
    private var webView: WebView

    init {
        LayoutInflater.from(context).inflate(R.layout.web_content, this, true)

        progressBar = findViewById(R.id.progress_bar)
        progressBar.apply {
            layoutParams = (layoutParams as LayoutParams).apply {
                addRule(CENTER_IN_PARENT, TRUE)
            }
        }

        webView = findViewById(R.id.web_view)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                onPageFinished()
            }
        }

        context.theme.obtainStyledAttributes(attrs, R.styleable.WebViewWithProgressBar, 0, 0)
            .apply {
                try {
                    html = getString(R.styleable.WebViewWithProgressBar_html) ?: ""
                } finally {
                    recycle()
                }
            }
    }

    private fun updateUI() {
        if (html.isEmpty()) {
            onPageFinished()
        } else {
            webView.isEnabled = false
            progressBar.visibility = View.VISIBLE
            webView.loadDataWithBaseURL(BASE_URL, adjustImagesCss + html, mimeType, encoding, null)
        }
    }

    private fun onPageFinished() {
        webView.isEnabled = true
        progressBar.visibility = View.GONE
    }

}