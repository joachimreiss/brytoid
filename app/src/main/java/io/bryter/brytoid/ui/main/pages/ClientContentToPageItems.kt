package io.bryter.brytoid.ui.main.pages

import io.bryter.brytoid.models.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.MultiInputPageInput
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputRequest.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse.*
import io.bryter.brytoid.ui.main.pages.InputPageItem.MultiInputPageItem
import io.bryter.brytoid.ui.main.pages.InputPageItem.SingleInputPageItem.*

fun clientContentToPageItems(clientContent: ClientContent): List<PageItem> {
    when (clientContent) {
        is ClientInProgressContent -> {
            return clientContent.pages.map {
                when (it) {
                    is ClientReadyForSubmissionPage -> ReadyForSubmissionPageItem
                    is ClientInputPage -> clientInputToInputPageInput(it)
                }
            }
        }
        is ClientFinishedContent -> {
            val page = clientContent.page
            return listOf(
                when (page) {
                    is ClientResultPage -> ResultPageItem(page.html)
                    is ClientRedirectPage -> RedirectPageItem(page.url)
                    is ClientHandoverSentPage -> HandoverSentPageItem(page.html, page.status)
                }
            )
        }
    }
}

fun clientInputToInputPageInput(clientInputPage: ClientInputPage): InputPageItem =
    clientInputPage.input.let { input ->
        return when (input) {

            is MultiClientInput ->
                MultiInputPageItem(
                    clientInputPage.id,
                    clientInputPage.html,
                    MultiInputPageInput(input.pages.map {
                        clientInputToInputPageInput(it)
                    }.map {
                        it.input
                    })
                )

            is SingleChoiceClientInput -> SingleChoiceInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = SingleChoiceInputPageInput(
                    request = SingleChoiceInputPageInputRequest(
                        input.request.choices,
                        input.request.isDropdown,
                        input.request.isCheckbox,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        SingleChoiceInputPageInputResponse(input.response.chosenId)
                    }
                )
            )
            is MultiSelectClientInput -> MultiSelectInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = MultiSelectInputPageInput(
                    request = MultiSelectInputPageInputRequest(
                        input.request.choices,
                        input.request.isDropdown,
                        input.request.isCheckbox,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        MultiSelectInputPageInputResponse(input.response.chosenIds)
                    }
                )
            )
            is TextClientInput -> TextInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = TextInputPageInput(
                    request = TextInputPageInputRequest(
                        input.request.placeholderContent,
                        input.request.regex,
                        input.request.characterLimit,
                        input.request.allowLinebreaks,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        TextInputPageInputResponse(input.response.text)
                    }
                )
            )
            is EmailAddressClientInput -> EmailAddressInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = EmailAddressInputPageInput(
                    request = EmailAddressInputPageInputRequest(
                        input.request.placeholderEmailAddress,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        EmailAddressInputPageInputResponse(input.response.emailAddress)
                    }
                )
            )
            is NumberClientInput -> NumberInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = NumberInputPageInput(
                    request = NumberInputPageInputRequest(
                        input.request.placeholderNumber,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        NumberInputPageInputResponse(input.response.number)
                    }
                )
            )
            is FileUploadClientInput -> FileUploadInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = FileUploadInputPageInput(
                    request = FileUploadInputPageInputRequest(
                        input.request.placeholderUploadButtonText,
                        input.request.fileCategory,
                        input.request.contentTypes,
                        input.request.allowMultipleFiles,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        FileUploadInputPageInputResponse(
                            input.response.blobId,
                            input.response.fileName,
                            input.response.contentType,
                            input.response.size
                        )
                    }
                )
            )
            is DatetimeClientInput -> DateInputPageItem(
                id = clientInputPage.id,
                html = clientInputPage.html,
                input = DatetimeInputPageInput(
                    request = DatetimeInputPageInputRequest(
                        input.request.datetimeFormat,
                        input.request.optional
                    ), response = if (input.response == null) null else {
                        DatetimeInputPageInputResponse(input.response.datetime)
                    }
                )
            )
        }
    }


fun inputPageInputToClientInput(
    clientContent: ClientContent,
    pageId: String,
    response: SingleInputPageInputResponse
): ClientInput {
    if (clientContent is ClientInProgressContent) {
        val filter: List<ClientInputPage> =
            clientContent.pages.filterIsInstance<ClientInputPage>()
        val page = filter.find {
            it.id == pageId
        }

        if (page != null) {
            if (page.input is SingleChoiceClientInput && response is SingleChoiceInputPageInputResponse) {
                return page.input.copy(
                    response = SingleChoiceClientInputResponse(
                        response.chosenId
                    )
                )
            }
        }
    }

    TODO()
}