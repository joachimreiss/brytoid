package io.bryter.brytoid.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.bryter.brytoid.BryterModuleAccessManager
import io.bryter.brytoid.models.*
import io.bryter.brytoid.network.main.MainApi
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse
import io.bryter.brytoid.ui.main.pages.inputPageInputToClientInput
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val mainApi: MainApi,
    private val bryterModuleAccessManager: BryterModuleAccessManager,
    private val publicEngineStateManager: PublicEngineStateManager
) : ViewModel() {

    fun observePublicEngineState(): LiveData<ApiResource<PublicEngineState>> {
        return publicEngineStateManager.stateLiveData
    }

    fun requestAndStartNewSession() {
        publicEngineStateManager.handleRequest(createRequestAndStartSessionLiveData())
    }

    fun resumeSession(sessionId: String) {
        publicEngineStateManager.handleRequest(createResumeSessionLiveData(sessionId))
    }

    fun sendPageResponse(pageId: String, response: SingleInputPageInputResponse) {
        publicEngineStateManager.handleRequest(createSendInputLiveData(pageId, response))
    }

    private fun createRequestAndStartSessionLiveData(): MutableLiveData<ApiResource<PublicEngineState>> {
        val mutableLiveData: MutableLiveData<ApiResource<PublicEngineState>> = MutableLiveData()
        bryterModuleAccessManager.currentPublishedModuleId?.let {
            mainApi.requestAndStartNewSession(it)
                .enqueue(sessionCallback(mutableLiveData))
        }
        return mutableLiveData
    }

    private fun sessionCallback(mutableLiveData: MutableLiveData<ApiResource<PublicEngineState>>) =
        object : Callback<RequestNewSessionResponseBody> {
            override fun onResponse(
                call: Call<RequestNewSessionResponseBody>,
                response: Response<RequestNewSessionResponseBody>
            ) {
                val (publishedModuleId, sessionId) = response.body()
                    ?: throw IllegalStateException("Empty body on successful response")
                mainApi.resumeSession(
                    ResumeSessionRequestBody(publishedModuleId, sessionId)
                ).enqueue(publicEngineStateCallback(mutableLiveData))
            }

            override fun onFailure(call: Call<RequestNewSessionResponseBody>, t: Throwable) {
                t.printStackTrace()
                mutableLiveData.value =
                    ApiResource.error("Error when trying to request new session")
            }
        }

    private fun createResumeSessionLiveData(sessionId: String): MutableLiveData<ApiResource<PublicEngineState>> {
        val mutableLiveData: MutableLiveData<ApiResource<PublicEngineState>> = MutableLiveData()
        bryterModuleAccessManager.currentPublishedModuleId?.let {
            mainApi.resumeSession(ResumeSessionRequestBody(it, sessionId))
                .enqueue(publicEngineStateCallback(mutableLiveData))
        }
        return mutableLiveData
    }


    private fun createSendInputLiveData(
        pageId: String,
        response: SingleInputPageInputResponse
    ): MutableLiveData<ApiResource<PublicEngineState>> {
        val mutableLiveData: MutableLiveData<ApiResource<PublicEngineState>> = MutableLiveData()

        val publishedModuleId = bryterModuleAccessManager.currentPublishedModuleId
        if (publishedModuleId != null) {
            val sessionId = publicEngineStateManager.stateLiveData.value?.data?.sessionId
            if (sessionId != null) {
                val clientContent = publicEngineStateManager.stateLiveData.value?.data?.content
                if (clientContent != null) {
                    val clientInput = inputPageInputToClientInput(clientContent, pageId, response)
                    val body = InputRequestBody(publishedModuleId, sessionId, pageId, clientInput)
                    mainApi.sendInput(body).enqueue(publicEngineStateCallback(mutableLiveData))
                }
            }
        }
        return mutableLiveData
    }

    private fun publicEngineStateCallback(mutableLiveData: MutableLiveData<ApiResource<PublicEngineState>>) =
        object : Callback<PublicEngineState> {

            override fun onResponse(
                call: Call<PublicEngineState>,
                response: Response<PublicEngineState>
            ) {
                val data = response.body()
                    ?: throw IllegalStateException("Empty body on successful response")
                mutableLiveData.value = ApiResource.success(data)
            }

            override fun onFailure(call: Call<PublicEngineState>, t: Throwable) {
                t.printStackTrace()
                mutableLiveData.value =
                    ApiResource.error("Error when trying to resume session")
            }
        }

}