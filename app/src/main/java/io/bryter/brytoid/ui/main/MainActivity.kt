package io.bryter.brytoid.ui.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ch.mobility.ridesharing.api.ApiStatus
import com.google.android.material.snackbar.Snackbar
import io.bryter.brytoid.BaseActivity
import io.bryter.brytoid.R
import io.bryter.brytoid.di.ViewModelProviderFactory
import io.bryter.brytoid.models.ApiResource
import io.bryter.brytoid.models.PublicEngineState
import io.bryter.brytoid.ui.main.pages.InputPageItem.InputPageInput.SingleInputPageInput.SingleInputPageInputResponse
import io.bryter.brytoid.ui.main.pages.PageItemAdapter
import io.bryter.brytoid.ui.main.pages.SendNewResponseListener
import io.bryter.brytoid.ui.main.pages.clientContentToPageItems
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseActivity(), SendNewResponseListener {
    private lateinit var viewModel: MainViewModel

    @JvmField
    @Inject
    var providerFactory: ViewModelProviderFactory? = null

    @JvmField
    @Inject
    var publicEngineStateManager: PublicEngineStateManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(view_pager) {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3
        }

        viewModel = ViewModelProvider(this, providerFactory!!).get(MainViewModel::class.java)
        viewModel.observePublicEngineState()
            .observe(
                this,
                Observer<ApiResource<PublicEngineState?>> {
                    updateUI(it)
                })

//        viewModel.resumeSession(DEBUG_SESSION_ID)
        viewModel.requestAndStartNewSession()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.logout -> {
            true
        }
        android.R.id.home -> {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START)
                true
            } else {
                false
            }
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun updateUI(resource: ApiResource<PublicEngineState?>) {
        when (resource.status) {
            ApiStatus.SUCCESS -> {
                progress_bar.visibility = View.GONE
                val pageItemAdapter = PageItemAdapter(this)
                view_pager.adapter = pageItemAdapter
                val list = clientContentToPageItems(resource.data!!.content)
                pageItemAdapter.updateContent(list)

                val lastItem =
                    if (pageItemAdapter.itemCount > 0) pageItemAdapter.itemCount - 1 else 0
                view_pager.currentItem = lastItem
                view_pager.visibility = View.VISIBLE
            }
            ApiStatus.ERROR -> {
                progress_bar.visibility = View.GONE
                Snackbar.make(
                    findViewById(android.R.id.content),
                    resource.message
                        ?: getString(R.string.common_error_message),
                    Snackbar.LENGTH_SHORT
                ).show()
            }
            ApiStatus.LOADING -> {
                progress_bar.visibility = View.VISIBLE
                view_pager.visibility = View.GONE
            }
        }
    }

    override fun sendNewResponse(
        pageId: String,
        response: SingleInputPageInputResponse
    ) {
        viewModel.sendPageResponse(pageId, response)
    }

}