package io.bryter.brytoid.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OAuthLoginRequestBody(val publishedModuleId: String)