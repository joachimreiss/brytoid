package io.bryter.brytoid.models;

import ch.mobility.ridesharing.api.ApiStatus


data class ApiResource<out T>(
    val status: ApiStatus,
    val data: T?,
    val message: String?
) {

    companion object {

        fun <T> success(newData: T? = null): ApiResource<T> =
            ApiResource(ApiStatus.SUCCESS, newData, null)

        fun <T> error(message: String, oldData: T? = null): ApiResource<T> =
            ApiResource(ApiStatus.ERROR, oldData, message)

        fun <T> loading(oldData: T? = null): ApiResource<T> =
            ApiResource(ApiStatus.LOADING, oldData, null)

    }

}