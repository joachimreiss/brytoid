package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResumeSessionRequestBody(
    @field:Json(name = "initStateData") val initStateData: InitStateData
) {
    constructor(
        publishedModuleId: String,
        sessionId: String
    ) : this(InitStateData(publishedModuleId, sessionId))
}