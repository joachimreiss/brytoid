package io.bryter.brytoid.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ModuleInfo(
    val moduleId: String,
    val name: String,
    val theme: String
)

@JsonClass(generateAdapter = true)
data class EngineError(
    val id: String,
    val message: String,
    val rawErrorMessage: String?,
    val httpErrorCode: Int?,
    val isFatal: Boolean
)

@JsonClass(generateAdapter = true)
data class PublicEngineState(
    val publishedModuleId: String,
    val sessionId: String,
    val engineBootedSuccessfully: Boolean,
    val locale: String,
    val moduleInfo: ModuleInfo,
    val remainingSteps: Map<String, Int>,
    val saveAndContinueLaterEnabled: Boolean,
    val content: ClientContent,
    val engineError: EngineError?
)