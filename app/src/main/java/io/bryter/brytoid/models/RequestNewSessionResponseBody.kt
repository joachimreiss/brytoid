package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RequestNewSessionResponseBody(
    @field:Json(name = "publishedModuleId") val publishedModuleId: String,
    @field:Json(name = "sessionId") val sessionId: String,
    @field:Json(name = "sensitive") val sensitive: Boolean
)