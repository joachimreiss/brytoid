package io.bryter.brytoid.models

import com.squareup.moshi.JsonClass

sealed class ClientContent(
    val type: ClientContentType
)

enum class ClientContentType {
    InProgress, Finished
}

@JsonClass(generateAdapter = true)
data class ClientInProgressContent(val pages: List<ClientInProgressPage>) :
    ClientContent(ClientContentType.InProgress)

@JsonClass(generateAdapter = true)
data class ClientFinishedContent(val page: ClientFinishedPage) :
    ClientContent(ClientContentType.Finished)


enum class ClientPageType {
    Input,
    ReadyForSubmission,
    HandoverSent,
    Result,
    Redirect
}

sealed class ClientFinishedPage(
    val type: ClientPageType
)

@JsonClass(generateAdapter = true)
data class ClientResultPage(val html: String) : ClientFinishedPage(ClientPageType.Result)

@JsonClass(generateAdapter = true)
data class ClientRedirectPage(val url: String) :
    ClientFinishedPage(ClientPageType.Redirect)

@JsonClass(generateAdapter = true)
data class ClientHandoverSentPage(
    val id: String,
    val html: String,
    val status: HandoverSentStatus
) : ClientFinishedPage(ClientPageType.HandoverSent)

enum class HandoverSentStatus {
    Success,
    AlreadyPerformed,
    Failure
}

sealed class ClientInProgressPage(
    val type: ClientPageType
)

@JsonClass(generateAdapter = true)
class ClientReadyForSubmissionPage : ClientInProgressPage(ClientPageType.ReadyForSubmission)

@JsonClass(generateAdapter = true)
data class ClientInputPage(
    val id: String,
    val html: String,
    val input: ClientInput
) : ClientInProgressPage(ClientPageType.Input)


sealed class ClientInput(
    open val type: ClientInputType
)

enum class ClientInputType {
    SingleChoice,
    MultiSelect,
    Text,
    EmailAddress,
    Number,
    FileUpload,
    Datetime,
    MultiInput
}

@JsonClass(generateAdapter = true)
data class MultiClientInput(
    val pages: List<ClientInputPage>
) : ClientInput(ClientInputType.MultiInput)

@JsonClass(generateAdapter = true)
data class SingleChoiceClientInput(
    val request: SingleChoiceClientInputRequest,
    val response: SingleChoiceClientInputResponse?
) : ClientInput(ClientInputType.SingleChoice)

@JsonClass(generateAdapter = true)
data class MultiSelectClientInput(
    val request: MultiSelectClientInputRequest,
    val response: MultiSelectClientInputResponse?
) : ClientInput(ClientInputType.MultiSelect)

@JsonClass(generateAdapter = true)
data class TextClientInput(
    val request: TextClientInputRequest,
    val response: TextClientInputResponse?
) : ClientInput(ClientInputType.Text)

@JsonClass(generateAdapter = true)
data class EmailAddressClientInput(
    val request: EmailAddressClientInputRequest,
    val response: EmailAddressClientInputResponse?
) : ClientInput(ClientInputType.EmailAddress)

@JsonClass(generateAdapter = true)
data class NumberClientInput(
    val request: NumberClientInputRequest,
    val response: NumberClientInputResponse?
) : ClientInput(ClientInputType.Number)

@JsonClass(generateAdapter = true)
data class FileUploadClientInput(
    val request: FileUploadClientInputRequest,
    val response: FileUploadClientInputResponse?
) : ClientInput(ClientInputType.FileUpload)

@JsonClass(generateAdapter = true)
data class DatetimeClientInput(
    val request: DatetimeClientInputRequest,
    val response: DatetimeClientInputResponse?
) : ClientInput(ClientInputType.FileUpload)

sealed class ClientInputRequest(
    open val optional: Boolean
)

sealed class ClientInputResponse

@JsonClass(generateAdapter = true)
data class Choice(val id: String, val label: String)

@JsonClass(generateAdapter = true)
data class SingleChoiceClientInputRequest(
    override val optional: Boolean,
    val choices: List<Choice>,
    val isDropdown: Boolean,
    val isCheckbox: Boolean
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class MultiSelectClientInputRequest(
    override val optional: Boolean,
    val choices: List<Choice>,
    val isDropdown: Boolean,
    val isCheckbox: Boolean
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class TextClientInputRequest(
    override val optional: Boolean,
    val placeholderContent: String,
    val regex: String?,
    val characterLimit: Int?,
    val allowLinebreaks: Boolean
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class EmailAddressClientInputRequest(
    override val optional: Boolean,
    val placeholderEmailAddress: String
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class NumberClientInputRequest(
    override val optional: Boolean,
    val placeholderNumber: String
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class FileUploadClientInputRequest(
    override val optional: Boolean,
    val placeholderUploadButtonText: String,
    val fileCategory: FileUploadCategory,
    val contentTypes: List<ContentType>,
    val allowMultipleFiles: Boolean
) : ClientInputRequest(optional)

@JsonClass(generateAdapter = true)
data class DatetimeClientInputRequest(
    override val optional: Boolean,
    val datetimeFormat: String
) : ClientInputRequest(optional)


@JsonClass(generateAdapter = true)
data class SingleChoiceClientInputResponse(val chosenId: String?) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class MultiSelectClientInputResponse(val chosenIds: List<String>?) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class TextClientInputResponse(val text: String?) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class EmailAddressClientInputResponse(val emailAddress: String?) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class NumberClientInputResponse(val number: Float?) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class FileUploadClientInputResponse(
    val blobId: String?,
    val fileName: String?,
    val contentType: ContentType?,
    val size: Float?
) : ClientInputResponse()

@JsonClass(generateAdapter = true)
data class DatetimeClientInputResponse(
    val datetime: String?
) : ClientInputResponse()


enum class FileUploadCategory {
    Document,
    Image,
    Custom
}
