package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OAuthLoginResponseBody(@field:Json(name = "token") val accessToken: String)