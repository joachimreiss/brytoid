package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ClientInputRequestData(
    @field:Json(name = "pageId") val pageId: String,
    @field:Json(name = "clientInput") val clientInput: ClientInput
)