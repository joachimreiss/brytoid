package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InputRequestBody(
    @field:Json(name = "initStateData") val initStateData: InitStateData,
    @field:Json(name = "requestData") val requestData: ClientInputRequestData

) {
    constructor(
        publishedModuleId: String,
        sessionId: String,
        pageId: String,
        clientInput: ClientInput
    ) : this(
        InitStateData(publishedModuleId, sessionId),
        ClientInputRequestData(pageId, clientInput)
    )
}