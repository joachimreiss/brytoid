package io.bryter.brytoid.models

data class ModuleAccess(val publishedModuleId: String, val accessToken: String)
