package io.bryter.brytoid.models

data class EngineSession(
    val publishedModuleId: String,
    val sessionId: String,
    val sensitive: Boolean
)
