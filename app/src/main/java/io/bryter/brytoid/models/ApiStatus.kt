package ch.mobility.ridesharing.api

enum class ApiStatus {

    SUCCESS,
    ERROR,
    LOADING

}
