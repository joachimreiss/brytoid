package io.bryter.brytoid.models

enum class ContentType(val value: String) {
    doc("application/msword"),
    xls("application/vnd.ms-excel"),
    ppt("application/vnd.ms-powerpoint"),
    docx("application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
    xlsx("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    pptx("application/vnd.openxmlformats-officedocument.presentationml.presentation"),
    odt("application/vnd.oasis.opendocument.text"), // odt (OpenOffice word-format)
    ods("application/vnd.oasis.opendocument.spreadsheet"), // ods (OpenOffice excel-format)
    odp("application/vnd.oasis.opendocument.presentation"), // odp (OpenOffice powerpoint-format)
    pages("application/vnd.apple.pages"), // macOS word-format
    numbers("application/vnd.apple.numbers"), // macOS excel-format
    keynote("application/vnd.apple.keynote"), // macOS powerpoint-format
    txt("text/plain"),
    csv("text/csv"),
    pdf("application/pdf"),
    jpeg("image/jpeg"),
    jpg("image/jpg"),
    png("image/png"),
    webp("image/webp"),
    gif("image/gif")
}