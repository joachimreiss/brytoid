package io.bryter.brytoid.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InitStateData(
    @field:Json(name = "publishedModuleId") val publishedModuleId: String,
    @field:Json(name = "sessionId") val sessionId: String,
    @field:Json(name = "requestUrl") val requestUrl: String,
    @field:Json(name = "featureToggleDictionary") val featureToggleDictionary: Map<String, String>
) {
    constructor(publishedModuleId: String, sessionId: String) : this(
        publishedModuleId,
        sessionId,
        "https://app.stage.bryter.io/s/$publishedModuleId",
        HashMap()
    )
}