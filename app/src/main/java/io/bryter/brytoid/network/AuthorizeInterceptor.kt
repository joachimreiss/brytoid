package io.bryter.brytoid.network


import io.bryter.brytoid.BryterModuleAccessManager
import io.bryter.brytoid.logWarn
import okhttp3.Interceptor
import okhttp3.Response


class AuthorizeInterceptor(val bryterModuleAccessManager: BryterModuleAccessManager) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val accessToken = bryterModuleAccessManager.currentModuleAccess.value?.data?.accessToken

        val requestBuilder = request.newBuilder()

        if (accessToken == null) {
            logWarn { "Auth required but not logged in" }
            return chain.proceed(requestBuilder.build())
        }

        return chain.proceed(
            requestBuilder
                .addHeader("Authorization", "Bearer $accessToken")
                .build()
        )
    }

}
