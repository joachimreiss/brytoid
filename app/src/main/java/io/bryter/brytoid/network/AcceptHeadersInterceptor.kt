package io.bryter.brytoid.network

import okhttp3.Interceptor
import okhttp3.Response

class AcceptHeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request().newBuilder()
                .header("Accept", "application/json")
                .build()
        )
    }

}
