package io.bryter.brytoid.network.auth

import io.bryter.brytoid.models.OAuthLoginRequestBody
import io.bryter.brytoid.models.OAuthLoginResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("api/internal/wizard/auth")
    fun auth(@Body body: OAuthLoginRequestBody): Call<OAuthLoginResponseBody>
}