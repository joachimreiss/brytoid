package io.bryter.brytoid.network.main

import io.bryter.brytoid.models.InputRequestBody
import io.bryter.brytoid.models.PublicEngineState
import io.bryter.brytoid.models.RequestNewSessionResponseBody
import io.bryter.brytoid.models.ResumeSessionRequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

interface MainApi {
    @POST("api/internal/wizard/{publishedModuleId}/sessions")
    fun requestAndStartNewSession(@Path("publishedModuleId") publishedModuleId: String): Call<RequestNewSessionResponseBody>

    @POST("api/engine/start")
    fun resumeSession(@Body body: ResumeSessionRequestBody): Call<PublicEngineState>

    @POST("api/engine/input")
    fun sendInput(@Body body: InputRequestBody): Call<PublicEngineState>
}