package io.bryter.brytoid

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.bryter.brytoid.di.DaggerAppComponent

class BaseApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerAppComponent.builder().application(this).build()

}